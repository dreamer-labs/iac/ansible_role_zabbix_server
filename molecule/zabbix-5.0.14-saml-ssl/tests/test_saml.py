import json
import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('zabbix')


def test_saml_auth(host):

    result = host.check_output(
        'source /home/centos/python_saml_test/bin/activate;'
        'python /home/centos/test_saml.py')
    assert 'Failed to redirect to IDP properly' not in result
    assert 'Authentication failed after redirect from IDP' not in result


def test_zabbix_web_api(host, zabbix_version):
    """
    Ensure API returns something.
    """
    request_data = {
            'jsonrpc': '2.0',
            'method': 'apiinfo.version',
            'id': 1,
            'auth': None,
            'params': {}
            }

    zabbix_api = host.run((
            f"curl localhost/api_jsonrpc.php "
            f"-H 'Content-Type: application/json-rpc' "
            f"-X POST "
            f"--data '{json.dumps(request_data)}'"
            ))

    expected_response = {
            'jsonrpc': '2.0',
            'result': f"{zabbix_version}",
            'id': 1
            }

    assert json.loads(zabbix_api.stdout) == expected_response


def test_zabbix_web_api_auth(host, ansible_vars):
    """
    Ensure API returns valid auth for defined credentials
    """
    admin = ansible_vars.get('zabbix_server_web_user', 'Admin')
    password = ansible_vars.get('zabbix_server_web_password')

    request_data = {
            'jsonrpc': '2.0',
            'method': 'user.login',
            'id': 1,
            'auth': None,
            'params': {
                'user': admin,
                'password': password
                }
            }

    zabbix_api = host.run((
            f"curl localhost/api_jsonrpc.php "
            f"-H 'Content-Type: application/json-rpc' "
            f"-X POST "
            f"--data '{json.dumps(request_data)}'"
            ))

    assert 'error' not in zabbix_api.stdout
    token = json.loads(zabbix_api.stdout)['result']
    assert len(token)

    request_data = {
            'jsonrpc': '2.0',
            'method': 'user.logout',
            'id': 1,
            'auth': token,
            'params': {}
            }

    host.run((
            f"curl localhost/api_jsonrpc.php "
            f"-H 'Content-Type: application/json-rpc' "
            f"-X POST "
            f"--data '{json.dumps(request_data)}'"
            ))
