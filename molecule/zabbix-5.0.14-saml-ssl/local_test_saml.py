from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_saml(driver):
    saml_base_url = 'localhost:8080/auth/realms/master/protocol/saml'
    monitoring_sso_url = 'https://localhost'

    driver.get(
        f"{monitoring_sso_url}/index_sso.php"
    )

    assert saml_base_url in driver.current_url, (
        "Failed to redirect to IDP properly"
    )

    username_field = driver.find_element_by_xpath('//*[@id="username"]')
    username_field.send_keys("testuser")
    password_field = driver.find_element_by_xpath('//*[@id="password"]')
    password_field.send_keys("test-password")
    button = driver.find_element_by_xpath('//*[@id="kc-login"]')
    button.click()

    wait = WebDriverWait(driver, 10)
    wait.until(EC.url_contains('zabbix.php?action=dashboard.view'))

    assert 'zabbix.php?action=dashboard.view' in driver.current_url, (
        "Failed to authenticate"
    )


if __name__ == '__main__':
    try:
        options = Options()
        options.add_argument("--headless")

        driver = webdriver.Firefox(options=options)
        test_saml(driver)
        print('tests passed')
        driver.close()
    except Exception as e:
        driver.close()
        raise e
