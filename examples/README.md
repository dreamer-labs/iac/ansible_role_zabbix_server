# Examples

### Quick Start

This example assumes python3 is available. It also assumes that
your server is accessible over SSH with the centos user and password-less sudo
is enable on the server.

```bash
python3 -m venv ansible
source ansible/bin/activate
pip install --upgrade pip
pip install -r ../requirements.txt -r ../test-requirements.txt
ansible-galaxy install -r ../requirements.yml
export SERVER_IP=<enter server IP>
cat <<- EOF > /tmp/inventory.ini
[zabbix]
$SERVER_IP
EOF
ansible-playbook -i /tmp/inventory.ini 5.0.8-without-ldap.yml
```
