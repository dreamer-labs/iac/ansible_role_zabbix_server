# ansible-role-zabbix-server

Deploys a zabbix server and web front end

## Requirements

This role requires a number of additional roles to deploy. Please refer to the `requirements.yml`

`ansible-galaxy install -r requirements.yml`

This role also assumes an already deployed database. Please reference the [galera repository](https://gitlab.com/dreamer-labs/iac/ansible_role_galera.git)

## Role Variables

### Required Variables

Note: You CANNOT use any special characters for the `zabbix_server_database_root_password` and `zabbix_server_database_root_password` variables.

| Variable                                | Default Value                | Explanation                                   |
|:----------------------------------------|:-----------------------------|:----------------------------------------------|
| zabbix_server_web_password              | None                         | Supply this variable in the playbook          |
| zabbix_server_web_url                   | NodeNoEvents                 | URL of the zabbix server                      |
| zabbix_server_database_root_password    | None                         | Supply this variable in the playbook          |
| zabbix_server_database_host             | None                         | Supply this variable in the playbook          |
| zabbix_server_database_root_user        | None                         | Supply this variable in the playbook          |
| zabbix_server_database_root_password    | None                         | Supply this variable in the playbook          |

### Optional Variables

Below are some optional variables for configuring Zabbix. You can find a complete list in `defaults/main.yml`

| Variable                              | Default Value                | Explanation                                   |
|:-----------------------------------   |:-----------------------------|:----------------------------------------------|
| zabbix_server_version                 | 4.4.10                       | The version of Zabbix Server to deploy        |
| zabbix_server_ldap_enable             | False                        | Enable LDAP configuration (or not)            |
| zabbix_server_ldap_port               | 389                          | Configures the LDAP port                      |
| zabbix_server_ldap_host               | None                         | The LDAP host to bind to                      |
| zabbix_server_ldap_base_dn            | None                         | LDAP Base search DN                           |
| zabbix_server_ldap_bind_dn            | None                         | LDAP Bind user (cn=Admin,dc=example,dc=org)   |
| zabbix_server_ldap_bind_password      | None                         | LDAP password to bind with                    |
| zabbix_server_ldap_search_attribute   | "sAMAccountName"             | ldap search attribute for looking up accounts |
| zabbix_server_repo_baseurl            | https://repo.zabbix.com      | Base URL for the zabbix repositories          |
| zabbix_server_database_deploy_host    | localhost                    | Deployment time Database Host Information     |
| zabbix_server_database_deploy_create  | True                         | Create the zabbix server database             |
| zabbix_server_database_type           | mysql                        | Type of the zabbix server database (mysql only) |
| zabbix_server_database_name           | zabbix-server                | Name of the zabbix database inside mysql      |
| zabbix_server_database_user           | zabbix-server                | User to connect to database with              |
| zabbix_server_database_host           | localhost                    | Should be `localhost` when mysql is hosted on the zabbix machine |
| zabbix_server_web_user                | Admin                        | Default web login name                        |
| zabbix_server_hostname                | "{{ inventory_hostname }}"   | Zabbix server hostname (zabbix.conf)          |
| zabbix_server_web_certs               | False                        | Dict TLS {cert: content, key: content, ca: content |
| zabbix_server_web_crt_path            | /etc/pki/server.crt          | Path to cert on the inventory host            |
| zabbix_server_web_key_path            | /etc/pki/server.key          | Path to key on the inventory host             |


## Example Playbook

See example in the `examples` directory.

## License

MIT

## Author Information

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
