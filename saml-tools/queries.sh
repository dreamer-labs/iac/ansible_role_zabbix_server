
get_token() {
  TOKEN=$(curl -X POST -H "application/x-www-form-urlencoded" http://127.0.0.1:8080/auth/realms/master/protocol/openid-connect/token \
          -d "grant_type=password&client_id=admin-cli&username=admin&password=admin" | jq -r '.access_token')
}

get_user() {
  curl -H "Authorization: Bearer ${TOKEN}" -H "Accept: application/json" \
            http://127.0.0.1:8080/auth/admin/realms/master/users?username=${1} | jq -r '.[0].id'
}

set_password() {
  curl --verbose -X PUT -H "Authorization: Bearer ${TOKEN}" -H "Content-Type: application/json" \
    -d  "{\"type\": \"password\", \"temporary\": false, \"value\": \"${2}\" }" \
    http://127.0.0.1:8080/auth/admin/realms/master/users/$(get_user ${1})/reset-password
}

get_client_mappers() {
  curl -H "Authorization: Bearer ${TOKEN}" -H "Accept: application/json" \
            "http://127.0.0.1:8080/auth/admin/realms/master/clients/${1}/protocol-mappers/models" | jq -r '.'
}

get_client() {
  curl -H "Authorization: Bearer ${TOKEN}" -H "Accept: application/json" \
            "http://127.0.0.1:8080/auth/admin/realms/master/clients/${1}" | jq -r '.'
}
