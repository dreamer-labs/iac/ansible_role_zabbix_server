# [2.5.0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.4.1...v2.5.0) (2021-10-18)


### Features

* remove logging on pw and cert tasks ([fc28d60](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/fc28d60))

## [2.4.1](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.4.0...v2.4.1) (2021-10-14)


### Bug Fixes

* restart fpm handler ([0e3c2b7](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/0e3c2b7))

# [2.4.0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.3.4...v2.4.0) (2021-09-29)


### Features

* Added saml authentication config + db query ([1085ca9](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/1085ca9))
* added testing for saml auth ([d756023](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/d756023))

## [2.3.4](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.3.3...v2.3.4) (2021-08-17)


### Bug Fixes

* removed non-package-provided uneeded custom service file ([0371676](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/0371676))

## [2.3.3](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.3.2...v2.3.3) (2021-08-17)


### Bug Fixes

* fixes for servicefile location + bad handler ([f11cc77](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/f11cc77))

## [2.3.2](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.3.1...v2.3.2) (2021-08-11)


### Bug Fixes

* bump cache size to 512M to fix zabbix crashing ([bb04d57](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/bb04d57))
* fixed test symlink + added test for auth ([5cfdd2f](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/5cfdd2f))
* password is now always set not just with LDAP ([55fb09c](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/55fb09c))

## [2.3.1](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.3.0...v2.3.1) (2021-08-09)


### Bug Fixes

* updated var namespace, ssl controls, examples ([ea70e91](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/ea70e91))

# [2.3.0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.2.0...v2.3.0) (2021-08-05)


### Features

* support 5.0.14, remove support 4.x.x ([fbf8721](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/fbf8721))

# [2.2.0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.1.0...v2.2.0) (2021-02-09)


### Features

* add DBTLS support ([e01dc06](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/e01dc06))

# [2.1.0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.0.3...v2.1.0) (2021-02-09)


### Bug Fixes

* fix ssl variables ([d1ff428](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/d1ff428))


### Features

* add zabbix 5 support ([8b19020](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/8b19020))

## [2.0.3](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.0.2...v2.0.3) (2021-02-04)


### Bug Fixes

* fix broken semodule configuration ([993879d](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/993879d))

## [2.0.2](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.0.1...v2.0.2) (2021-02-04)


### Bug Fixes

* authorize users on all IPs ([943f83f](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/943f83f))

## [2.0.1](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v2.0.0...v2.0.1) (2021-01-12)


### Bug Fixes

* Add logic handling no ldap configuration ([cb840b1](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/cb840b1))
* add requirements.txt ([5cc9fc5](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/5cc9fc5))
* fix badly named variable ([2862f2a](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/2862f2a))
* move requirements.yml ([9d72737](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/9d72737))

# [2.0.0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v1.3.1...v2.0.0) (2021-01-12)


### Bug Fixes

* improve mysql security ([dcd560a](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/dcd560a))


### Features

* secure variables using our best practices ([781a2fc](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/781a2fc))


### BREAKING CHANGES

* properly namespace variable names

This introduces a different paradigm for securing variables
and ensuring that required variables are passed to the playbook
at execution time.

## [1.3.1](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v1.3.0...v1.3.1) (2021-01-07)


### Bug Fixes

* more security focused fixes ([5fced2e](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/5fced2e))

# [1.3.0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v1.2.0...v1.3.0) (2021-01-07)


### Bug Fixes

* implement better idempotency checks for mysql ([e4f73c0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/e4f73c0))
* migrate filter_plugins to common dir ([aca77c0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/aca77c0))
* restart zabbix and apache services ([a19d9c9](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/a19d9c9))
* update zabbix service file ([7c128d3](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/7c128d3))
* Version locking packages and test ([a146655](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/a146655))
* zabbix gpgkey(s) enabled and removed dependency ([6a73bd3](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/6a73bd3))


### Features

* Add LDAP authentication testing ([bbdfbc3](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/bbdfbc3))
* rename variables to ensure proper scoping ([e9d220d](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/e9d220d))

# [1.2.0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v1.1.0...v1.2.0) (2020-06-09)


### Bug Fixes

* fix a missing zabbix_enable_ldap variable and linting issue ([122cb82](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/122cb82))
* run scripts ([322dd03](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/322dd03))


### Features

* adding ldap ([7bba4b5](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/7bba4b5))

# [1.1.0](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/compare/v1.0.0...v1.1.0) (2020-03-20)


### Features

* adding to disable/enable HA ([f0329f5](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/f0329f5))

# 1.0.0 (2020-03-19)


### Bug Fixes

* Add selinux boolean httpd_can_connect_ldap ([db1e42c](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/db1e42c))
* Import all previous commits from the B network ([2131121](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/2131121))
* Remove the requirement for apache role ([a04f8e6](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/a04f8e6)), closes [#2](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/issues/2)


### Features

* Update gitlab ci to use repoman tempates and generate container ([6baf721](https://gitlab.com/dreamer-labs/iac/ansible_role_zabbix_server/commit/6baf721))
