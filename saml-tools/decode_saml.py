import sys
import zlib
import urllib.parse as urlParse
import base64


def decode_authn_request(authn_request):
    """ AuthnRequest is always deflated, base64 encoded and url-escaped.

        :Parameters:
        -`authn_request`: AuthnRequest

        :Return:
         The decoded AuthnRequest if successful else empty string.
    """
    decoded = ''
    a = urlParse.unquote(authn_request)

    try:
        decoded = zlib.decompress(base64.b64decode(a), -8)
    except (zlib.error, TypeError):
        try:
            decoded = zlib.decompress(base64.b64decode(a))
        except (zlib.error, TypeError):
            pass
    return decoded.decode('utf8')


def encode_authn_request(authn_request_xml):
    """ AuthnRequest is always deflated, base64 encoded and url-escaped.

        :Parameters:
        -`authn_request`: AuthnRequest

        :Return:
         The encoded AuthnRequest if successful else empty string.
    """
    encoded = base64.b64encode(zlib.compress(authn_request_xml.encode()))
    return urlParse.quote(encoded.decode('ascii'))


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser('authn-tool')

    parser.add_argument('--encode',
                        action='store_true')

    parser.add_argument('--decode',
                        action='store_true',
                        default=True)

    opts = parser.parse_args()

    if opts.encode:
        print(encode_authn_request(sys.stdin.read()))
    else:
        print(decode_authn_request(sys.stdin.read()))
